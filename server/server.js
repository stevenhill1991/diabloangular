var Hapi = require('hapi');
var Good = require('good');
var Path = require('path');

var serverConfig = require('./server.config.js');

var server = new Hapi.Server();

server.connection({
    host: serverConfig.server.host,
    port: serverConfig.server.port
});

// load api routes using index.js file export
require('./routes')(server);

server.route({
    method: 'GET',
    path: '/assets/{param*}',
    handler: {
        directory: {
            path: Path.join(__dirname,'./assets'),
            listing: true // show a file listing at assets/
        }
    }
});

server.route({
    method: 'GET',
    path: '/',
    handler: function(request, reply){
        return reply.file(Path.join(__dirname,'./index.html'));
    }
});

// final catch all, redirect home
server.route({
    method: '*',
    path: '/{p*}',
    handler: function(request, reply){
        return reply.redirect('/');
    }
});

server.register({
    register: Good,
    options: {
        reporters: [{
            reporter: require('good-console'),
            events: {
                response: '*',
                log: '*'
            }
        }]
    }
}, function (err) {
    if (err) {
        throw err; // something bad happened loading the plugin
    }

    server.start(function () {
        server.log('info', 'Server running at: ' + server.info.uri);
    });
});