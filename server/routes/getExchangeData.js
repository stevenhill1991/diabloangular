/**
 * Created by steven.hill on 18/08/2015.
 */
var http = require('http');
var serverConfig = require('../../public/server.config.js');
var request = require('request');

module.exports = function(server){
    server.route({
        method: 'GET',
        path: '/api/exchange',
        handler: getExchangeData
    });


    function getExchangeData(request, reply){
        getExchangeDataHttp(function(err, data){
            if(err){
                return reply(new Error(err));
            }
            return reply(JSON.parse(data)).type('application/json');
        });
    }

    function getExchangeDataHttp(callback){
        var url = 'https://rsbuddy.com/exchange/summary.json';
        request(url,function(error, response, body){
            if(error){
                callback(error);
            }
            if(!error && response.statusCode ==200){
                callback(null, body);
            }
        });
    }
};