var http = require('http');
var serverConfig = require('../server.config.js');
var request = require('request');

module.exports = function(server){
    server.route({
        method: 'GET',
        path: '/api/profile/{battleTag}/hero/{heroId}',
        handler: getHeroData
    });


    function getHeroData(request, reply){
        var battleTag = request.params.battleTag;
        var heroId = request.params.heroId;
        if(!battleTag){
            return reply.redirect('/');
        }
        getHeroDataHttp(battleTag, heroId, function(err, data){
            if(err){
                return reply(new Error(err));
            }
            return reply(data).type('application/json');
        });
    }

    function getHeroDataHttp(battleTag, heroId, callback){
        var key = serverConfig.battleNet.apiKey;
        var tag =  encodeURIComponent(battleTag);
        var url = 'https://eu.api.battle.net/d3/profile/' + tag + '/hero/' + heroId +'?locale=en_GB&apikey=' + key;
        request(url,function(error, response, body){
            if(error){
                callback(error);
            }
            if(!error && response.statusCode ==200){
                callback(null, body);
            }
        });
    }
};
