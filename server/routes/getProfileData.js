var http = require('http');
var serverConfig = require('../server.config.js');
var request = require('request');


module.exports = function(server){
  server.route({
      method: 'GET',
      path: '/api/profile/{battleTag?}',
      handler: getProfileData
  });


    function getProfileData(request, reply){
        var battleTag = request.params.battleTag;
        if(!battleTag){
            return reply.redirect('/');
        }
        getProfileDataHttp(battleTag, function(err, data){
            if(err){
                return reply(err);
            }
            server.log("i am returning valid data to angular", data);
            return reply(data).type('application/json');
        });
    }

    function getProfileDataHttp(battleTag, callback){
        var key = serverConfig.battleNet.apiKey;
        var tag =  encodeURIComponent(battleTag);
        var url = 'https://eu.api.battle.net/d3/profile/'+ tag + '/?locale=en_GB&apikey=' + key;
        request(url,function(error, response, body){
            if(error){
                server.log("calling back with error");
                return callback(error);
            }
            if(response.statusCode ==200){
                return callback(null, body);
            }
        });
    }
};
