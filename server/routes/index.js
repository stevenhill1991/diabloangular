module.exports = function(server){
  require('./getProfileData')(server);
  require('./getHeroData')(server);
  require('./getExchangeData')(server);
};