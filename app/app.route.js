
(function(){
	'use strict';

	angular
		.module('app')
		.config(config);

	config.$inject = ['$stateProvider', '$urlRouterProvider'];

	function config($stateProvider, $urlRouterProvider){

		$urlRouterProvider.otherwise('/');

		$stateProvider
			.state('search',{
				url:'/',				
				templateUrl: 'assets/templates/diabloArmorySearch/diabloArmorySearch.html',
				controller: 'diabloArmorySearch'			
			})
			.state('search.profile',{
				url:'profile/{battleTag}',
				templateUrl: 'assets/templates/diabloArmorySearchProfile/diabloArmorySearchProfile.html',
				controller: 'diabloArmorySearchProfile'				
			})
			.state('search.profile.hero',{
				url:'/hero/{heroId}',
				templateUrl: 'assets/templates/diabloArmorySearchHero/diabloArmorySearchHero.html',
				controller: 'diabloArmorySearchHero'
			});

		/*
		$routeProvider
			.when('/', {
				templateUrl: 'assets/templates/diabloArmorySearch.html',
				controller: 'diabloArmorySearch',
				controllerAs: 'vm'
			})
			.otherwise({
				redirectTo: '/'
			});
			*/


	}

})();