(function(){
    'use strict';


describe('diablo api service', function(){

    var diabloService;

    beforeEach(module('app'));

    beforeEach(function() {
        angular.mock.inject(function($injector) {
            diabloService = $injector.get('diabloService');
        });
    });

    it('should be defined', function(){
        expect(diabloService).toBeDefined();
    });
    /*

    describe('function: getProfileData', function(){
        it('should return profile data for a valid battle tag', function(){

        });
        it('should return err data for a invalid battle tag', function(){
            expect(diabloService.getProfileData('invalid#1234')).toBe();
        });
    });

    */

    describe('function: validTag', function(){
        it('should accept a valid english battle tag', function(){
            expect(diabloService.validTag('test#1234')).toBe(true);
        });
        it('should accept korean characters', function(){
            expect(diabloService.validTag('가우a1332ff리ab#1234')).toBe(true);
        });
        it('should accept a valid korean battletag', function(){
            expect(diabloService.validTag('무료찌찌#3111')).toBe(true);
        });
        it('should accept a valid taiwan battletag', function(){
            expect(diabloService.validTag('토니스타크#3358')).toBe(true);
    });
        it('should accept a battletag of length 3', function(){
            expect(diabloService.validTag('t12345678911#1234')).toBe(true);
        });
        it('should accept a battletag of length 12', function(){
            expect(diabloService.validTag('t12345678911#1234')).toBe(true);
        });
        it('should reject a battletag starting with a number', function(){
            expect(diabloService.validTag('1test#1234')).toBe(false);
        });
        it('should reject a battletag with less than 3 leading characters', function(){
            expect(diabloService.validTag('ab#1234')).toBe(false);
        });
        it('should reject a battletag with 13 leading characters', function(){
            expect(diabloService.validTag('abcdefghijklm#1234')).toBe(false);
        });
        it('should reject a battletag with a double hashtag', function(){
            expect(diabloService.validTag('test##1234')).toBe(false);
        });
        it('should reject a battletag with more than four ending numbers', function(){
            expect(diabloService.validTag('test#12345')).toBe(false);
        });
    });

    describe('function: validCountryCode', function(){
        it('should accept a valid country code', function(){
            expect(diabloService.validCountryCode('EU')).toBe(true);
        });
        it('should reject an invalid country code', function(){
            expect(diabloService.validCountryCode('AA')).toBe(false);
        });
    });
});

})();