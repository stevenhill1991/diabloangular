(function(){
   'use strict';

    angular
        .module('app')
        .factory('diabloService', diabloService);

    diabloService.$inject = ['$http','$q','$log'];

    function diabloService($http,$q,$log){

        var service = {
            getProfileData: getProfileData,
            getHeroData: getHeroData,
            validTag: validTag,
            validCountryCode: validCountryCode
        };

        return service;

        function getProfileData(battleTag){
            var defer = $q.defer();

            var tag = validTag(battleTag);
            if(!tag) {
                defer.reject('Invalid Tag please use format 1[a-z]11[a-z0-9]#4[0-9]');
                return defer.promise;
            }

            var splitTag = battleTag.split('#').join('-');
            var url = '/api/profile/'+ splitTag;
            $http({
                method: 'GET',
                url: url
            })
               .success(function(data){
                    if(data.reason){
                        defer.resolve(data.reason);
                    }
                    defer.resolve(data);               
                })
                .error(function(err){
                    defer.reject(err);
                });

            return defer.promise;
        }

        function getHeroData(battleTag, heroId){
            var defer = $q.defer();

            var tag = validTag(battleTag);
            if(!tag) {
                defer.reject('Invalid Tag please use format 1[a-z]11[a-z0-9]#4[0-9]');
                return defer.promise;
            }
            var splitTag = battleTag.split('#').join('-');
            var url = '/api/profile/'+ splitTag +'/hero/'+heroId;

            $http({
                method: 'GET',
                url: url
                })
                .success(function(data){
                    defer.resolve(data);
                })
                .error(function(err){
                    defer.reject(err);
                });

            return defer.promise;
        }

        function validTag(battleTag){
            var pattern = new RegExp('^([a-zA-Z가-힣][a-zA-Z0-9가-힣]{2,11}(#|-)[0-9]{4})$');
            return pattern.test(battleTag);
        }

        function validCountryCode(countryCode){
            var codes = ['EU', 'KR', 'TW', 'US'];
            return (codes.indexOf(countryCode)!=-1);
        }

    }

})();