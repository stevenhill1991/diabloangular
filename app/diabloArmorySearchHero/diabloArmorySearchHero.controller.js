(function(){
    'use strict';

    angular
        .module('app')
        .controller('diabloArmorySearchHero', diabloArmorySearchHero);

    diabloArmorySearchHero.$inject = ['$scope', 'diabloService','$log','$stateParams', '$state'];

    function diabloArmorySearchHero($scope, diabloService, $log,$stateParams, $state){

        diabloService.getHeroData($stateParams.battleTag, $stateParams.heroId)
            .then(function(data) {
                $log.log(data);
                $scope.heroData = data;
            }, function(err){
                $log.log(err);
            });
    }

})();