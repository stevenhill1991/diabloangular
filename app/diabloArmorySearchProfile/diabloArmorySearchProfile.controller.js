(function(){
    'use strict';

    angular
        .module('app')
        .controller('diabloArmorySearchProfile', diabloArmorySearchProfile);

    diabloArmorySearchProfile.$inject = ['$scope', 'diabloService','$log','$stateParams', '$state','$rootScope'];

    function diabloArmorySearchProfile($scope, diabloService, $log,$stateParams, $state,$rootScope){


        if(!$scope.$parent.profileData){
            $scope.$parent.searchError  = '';
            $scope.$parent.battleTag = $stateParams.battleTag;
            $scope.isSubmit = true;

            diabloService.getProfileData($stateParams.battleTag, $scope.selected)
                .then(function(data) {
                    $log.log(data);
                    if (data.battleTag) {
                        $log.log("valid search loading data", data);
                        $scope.profileData = data;
                        $scope.isSubmit = false;
                    } else {
                        $log.log("invalid search - navigating home",data);
                        $scope.$parent.searchError = data;
                        $state.go('search', {location: true});
                    }
                }, function(err){
                    $log.log("invalid search - navigating home");
                    $scope.$parent.searchError = err.message;
                    $scope.isSubmit = false;
                    $state.go('search',{location: true});
                });

        }

    }

})();