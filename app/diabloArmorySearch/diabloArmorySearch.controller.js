(function(){
    'use strict';

    angular
        .module('app')
        .controller('diabloArmorySearch', diabloArmorySearch);

    diabloArmorySearch.$inject = ['$scope', 'diabloService','$log','$state'];

    function diabloArmorySearch($scope, diabloService, $log, $state){

        $scope.battleTag = 'example#1234';
        $scope.selected = 'EU';
        $scope.selectedData  = ['EU', 'KR', 'TW', 'US'];

        $scope.search = function(){
            $scope.searchError = '';
            $log.log("clicked");
            $scope.isSubmit = true; 

            diabloService.getProfileData($scope.battleTag, $scope.selected)
                .then(function(data){
                    if (data.battleTag) {
                        $scope.profileData = data;
                        $scope.isSubmit = false;
                        $state.go('search.profile', {battleTag: $scope.battleTag},{location: true});
                    } else {
                        $scope.searchError = data;
                        $state.go('search');
                    }
                }, function(err){
                    $scope.searchError = err;
                    $scope.isSubmit = false;
                });
        };

    }

})();