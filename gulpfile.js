var gulp = require('gulp'),
	del = require('del'),
	jshint = require('gulp-jshint'),
	stylish = require('jshint-stylish'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	sourcemaps = require('gulp-sourcemaps'),
	karma = require('gulp-karma'),
	mainBowerFiles = require('main-bower-files'),
	runSequence = require('run-sequence'),
	server = require( 'gulp-develop-server' )

	testFiles = [
		'./public/assets/javascript/angular.js',
		'./public/assets/javascript/angular-mocks.js',
		'./public/assets/javascript/angular-route.js',
		'./public/assets/javascript/app.js',
		'./app/**/*.spec.js'
	],
	serverFiles = {
		index: './server/server.js',
		files: './server/**/*.js',
		ignore: '!./server/server.js'
	},
	input = {
		'modules': './app/**/*.module.js',
		'config': './app/**/*.config.js',
		'routes': './app/**/*.route.js', 
		'javascript': './app/**/*.js',
		'serverjs': './server/**/*.js',
		'index': './app/index.html',
		'views': './app/**/*.html',
		'ignore': '!/**/*.spec.js'
	},
	output = {
		'stylesheets': 'public/assets/css',
		'javascript': 'public/assets/javascript',
		'index': 'public',
		'views': 'public/assets/templates',
		'serverRoutes': 'public',
		'server': 'public'
	};

gulp.task('default', ['watch']);

gulp.task('clean', function(done){
	del(['public'], done);
});

gulp.task('jshint',function(){
	return gulp.src(input.javascript)
		.pipe(jshint())
		.pipe(jshint.reporter(stylish));
});

gulp.task('bower', function(){
	return gulp.src(mainBowerFiles())
		.pipe(gulp.dest(output.javascript));
});

gulp.task('build-js', function(){
	return gulp.src([
			input.modules,
			input.config,
			input.routes,
			input.javascript,
			input.ignore
		])		
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(gulp.dest(output.javascript));
});

gulp.task('templates', function(){
	return gulp.src(input.index)
		.pipe(gulp.dest(output.index));
});

gulp.task('views', function(){
	return gulp.src([
		'!app/index.html',
		input.views
		])
		.pipe(gulp.dest(output.views));
});


gulp.task('karma-test', function(){
	return gulp.src(testFiles)
		.pipe(karma({
			configFile: 'karma.conf.js',
			action: 'run'
		}))
		.on('error', function(err){
			throw err;
		});
});

gulp.task('build-server', function(){
	gulp.src(serverFiles.index)
		.pipe(gulp.dest(output.server));
	gulp.src([
		serverFiles.files,
		serverFiles.ignore
	])
		.pipe(gulp.dest(output.serverRoutes));
});

gulp.task('images', function(){
	return gulp.src('d3images/*.png')
		.pipe(gulp.dest('public/assets/images'));
})

gulp.task('start-server', function(){
	server.listen({ path: './public/server.js'})
});

gulp.task('test', function(){
	runSequence('clean', 'jshint', 'build-js', 'bower', 'karma-test');
});

gulp.task('dev', function(){
	gulp.watch([
		input.javascript,
		input.serverjs
	], function(){
		runSequence('clean', 'jshint', 'build-js', 'bower','karma-test','templates', 'views','build-server');
	});
});

gulp.task('build', function(){
		runSequence('clean', 'jshint', 'build-js', 'bower','templates','views','build-server','images');
});
